use clap::Parser;
use either::Either;
use itertools::Itertools;
use sha1::{Digest, Sha1};
use tabled::{Table, Tabled};

fn main() -> Result<(), Box<dyn std::error::Error>>
{
    let args = Args::parse();
    let hash_substr = get_hash_substr(args.pass.as_bytes());

    let password_api_response =
        call_passwords_api(&hash_substr).expect("Calling the Password API failed");

    let occurrences = turn_to_occurrence_map_list(&password_api_response, &args);

    let table_data = occurrences
        .iter()
        .enumerate()
        .map(|(index, occurrence)| ResultDisplay {
            rank: (index + 1) as u32,
            hash: &occurrence.hash,
            hack_attempts: occurrence.occurrences,
        })
        .collect::<Vec<_>>();

    let table = Table::new(table_data).to_string();

    let total_matches = password_api_response.lines().count();
    println!("Number of probable matches: {total_matches}");
    println!("{table}");

    Ok(())
}

fn get_hash_substr(password: &[u8]) -> String
{
    let password_hash = Sha1::new().chain_update(password).finalize();
    hex::encode(password_hash)
        .chars()
        .take(5)
        .map(|char| char.to_string())
        .collect::<Vec<_>>()
        .join("")
}

fn call_passwords_api(hash_substring: &str) -> reqwest::Result<String>
{
    if hash_substring.len() > 5
    {
        panic!("Hash substring must not be more than 5 characters")
    }

    reqwest::blocking::get(format!(
        "https://api.pwnedpasswords.com/range/{}",
        hash_substring
    ))?
    .text()
}

fn turn_to_occurrence_map_list(str: &str, args: &Args) -> Vec<HashOccurrenceMap>
{
    let pairs = str
        .lines()
        .map(|line| {
            let line_breakdown = line.split(':').take(2).collect::<Vec<_>>();
            let hash = line_breakdown.first().unwrap();
            let occurrences = line_breakdown.get(1).unwrap().parse::<u32>().unwrap();
            (*hash, occurrences)
        })
        .sorted_by_key(|(_, occurrences)| *occurrences)
        .map(|pair| -> HashOccurrenceMap { pair.into() });

    let results = if args.asc
    {
        Either::Left(pairs.take(args.take))
    }
    else
    {
        Either::Right(pairs.rev().take(args.take))
    };

    results.collect::<Vec<_>>()
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about=None)]
struct Args
{
    /// Your password
    #[arg(short, long)]
    pass: String,

    #[arg(short, long, default_value_t = 10)]
    take: usize,

    #[arg(short, long, default_value_t = false)]
    asc: bool,
}

#[derive(Debug)]
struct HashOccurrenceMap
{
    hash: String,
    occurrences: u32,
}

#[derive(Debug, Tabled)]
struct ResultDisplay<'a>
{
    rank: u32,
    hash: &'a str,
    hack_attempts: u32,
}

impl HashOccurrenceMap
{
    pub fn new(hash: &str, occurrences: u32) -> Self
    {
        Self {
            hash: hash.to_string(),
            occurrences,
        }
    }
}

impl From<(&str, u32)> for HashOccurrenceMap
{
    fn from((hash, occurrences): (&str, u32)) -> Self
    {
        HashOccurrenceMap::new(hash, occurrences)
    }
}
